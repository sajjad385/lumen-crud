<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Problem data Store</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand" href="#">Problem </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#"  style="color: #e1e1e1;" >Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="create.php"  style="color: #0fa9e1; font-weight: bolder" >Add Data</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<div class="container-fluid">


<table  id="list_table" class="table table-bordered table-striped table-light text-center">

        <tr class="bg-success">
            <th scope="col" class="col-md-1">ID</th>
            <th scope="col" class="col-md-2">Title</th>
            <th scope="col" class="col-md-1">Created By</th>
            <th scope="col" class="col-md-1">Modified By</th>
            <th scope="col" class="col-md-2">Action</th>
        </tr>

</table>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"       aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Problem Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="modalForm">
                        <label for="">Title </label>
                        <input type="text" class="form-control" id="title" name="title" value="" placeholder="Enter Problem Title ">
                        <label for="">Created_by </label>
                        <input type="text" class="form-control" id="created_by" name="created_by" value="" placeholder="Enter Created By ">
                        <label for="">Modified By </label>
                        <input type="text" class="form-control" id="modified_by" name="modified_by" value="" placeholder="Enter Modified By ">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <input type="text" id="problem_id" value="" hidden>
                    <button id="update" type="submit" class="btn btn-success">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <footer class="text-center container-fluid" style="background: steelblue; padding: 20px; margin: -17px 0 0">Developed by MSHS</footer>


</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" ></script>

<script type="text/javascript">
    function index() {
        $.ajax({
            url : 'http://problem-reason.org/problem',
            type : 'get',
            success : function (response) {
               let  result = eval(response);
                 for (let item of result){
                    let output = '<tr class="old-tr">\n' +
                        '    <td>'+item.id+'</td>\n' +
                        '    <td>'+item.title+'</td>\n' +
                        '    <td>'+item.created_by+'</td>\n' +
                        '    <td>'+item.modified_by+'</td>\n' +
                        '    <td><button id="'+item.id+'" class="btn btn-info problemEdit">Edit</button> || ' +
                        '       <button onclick="del('+item.id+')" class="btn btn-danger">Delete</button>'+""+'</td>\n' +
                        '</tr>';
                    $("#list_table").append(output);
                }
            }
        })
    }

    index();

    $(document).on('click', '.problemEdit', (e)=>{
        $("#exampleModal").modal();
        edit(e.target.id);
    });
    function edit(id){
        $.ajax({
                type : "get",
                url : 'http://problem-reason.org/problem/'+id,
                success : (data) =>{
                console.log(data);
               $("#problem_id").val(data.id);
               $("#title").val(data.title);
               $("#created_by").val(data.created_by);
               $("#modified_by").val(data.modified_by);
            }
        });
    }

    function update(){
        let modified_by = $("#modified_by").val();
        let created_by = $("#created_by").val();
        let title = $("#title").val();
        let id = $("#problem_id").val();
        $.ajax({
                type : "put",
                data : {title, created_by, modified_by},
                url : 'http://problem-reason.org/problem/'+id,
                success : (data) =>{
                    $(".old-tr").remove();
                    index();
                }/*,
                error: function (request, status, error) {
                    alert(request.responseText);
                }*/
        });
    }
    $(document).on('submit','#modalForm',(e) =>{
        update();
        e.preventDefault();
    })

    function del(id){
//        console.log(id);
        if(confirm('Aer you sure want to delete ?')){
            $.ajax({
                    type : "delete",
                    url : 'http://problem-reason.org/problem/'+id,
                    success : (data) =>{
                        $(".old-tr").remove();
                        index();
                    }
            });
        }
        else{
            return false;
        }

    }




</script>





<script>
    $(document).on('click', '.edit' ,function() {
        var id = this.id;
        var split_id = id.split("_");
        var field_name = split_id[0];
        var edit_id = split_id[1];
        var value = $(this).text();

        var ttruyen =  $('#ttruyen').text();
        var tgia =  $('#tgia').text();
        var ttat =  $('#ttat').text();
        var ndung =  $('#ndung').text();
        $.ajax ({
            url : "modules/favorites/edit.php",
            type : "POST",
            dataType : "text",
            data : {
                tentruyen: ttruyen, tacgia: tgia, id:edit_id , tomtat : ttat, noidung : ndung
            },
            success: function (data) {
                $('#alert_message').html("<div class='col-sm-4 alert alert-success' ><b>Edit</b> data successfully</div>");
                fetchdata(data);
            }
        });
        setInterval(function() {
            $("#alert_message").html('');
        }, 3000);
    });


</script>


</body>
</html>