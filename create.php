<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Store data</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php" style="color: #0fa9e1; font-weight: bolder" >Back to Home <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<div class="container">
    <form id="myform" onsubmit="create()" enctype="multipart/form-data" method="post" action="" >
        <div class="form-group">

            <label for="">Title </label>
            <input type="text" class="form-control" id="title" name="title" value="" placeholder="Enter Problem Title ">
            <label for="">Created_by </label>
            <input type="text" class="form-control" id="created_by" name="created_by" value="" placeholder="Enter Created By ">
            <label for="">Modified By </label>
            <input type="text" class="form-control" id="modified_by" name="modified_by" value="" placeholder="Enter Modified By ">

        </div>
        <button id="create" type="submit" class="btn btn-primary">Submit</button>
    </form>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<script type="text/javascript">
    function create() {

        let title = $("#title").val();
        let created_by = $("#created_by").val();
        let modified_by = $("#modified_by").val();

        let problems = {
            title : title,
            created_by : created_by,
            modified_by : modified_by,
        }
//        console.log(problems);

        $.ajax({
            url : 'http://problem-reason.org/problem',
            type : 'post',
            data : problems,
            success : function (response) {
                if(response){
                    alert("Successfully Inserted Data");
                }
            }

        })
//        event.preventDefault();
    }
//    document.getElementById('create').addEventListener('click', create, false);

    document.getElementById('myform').addEventListener('submit', function (e) {
        create();
        e.preventDefault();
    });









</script>
</body >
</html >